#!/bin/ash
PIHOLE_IP="192.168.1.4"
VAR="false"
PIHOLE1="iptables -t nat -A PREROUTING -i br-lan ! -s "$PIHOLE_IP" -p udp --dport 53 -j DNAT --to "$PIHOLE_IP""
PIHOLE2="iptables -t nat -A PREROUTING -i br-lan ! -s "$PIHOLE_IP" -p tcp --dport 53 -j DNAT --to "$PIHOLE_IP""
PIHOLE3="iptables -A INPUT -p udp --dport 53 -j DROP"
PIHOLE4="iptables -A INPUT -p tcp --dport 53 -j DROP"
echo Starting checker script ...

        /etc/init.d/firewall restart
        eval "$PIHOLE1"
        eval "$PIHOLE2"
        eval "$PIHOLE3"
        eval "$PIHOLE4"

while true;
do
        if ping -c1 -W 1 "$PIHOLE_IP" > /dev/null && curl -sL http://"$PIHOLE_IP"/admin > /dev/null && ! curl -sL http://"$PIHOLE_IP"/admin | grep "DNS service not running" > /dev/null
        then
                if [ "$VAR" = "true" ]
                then
                        echo failing back over...
                        eval "$PIHOLE1"
                        eval "$PIHOLE2"
                        eval "$PIHOLE3"
                        eval "$PIHOLE4"
                        VAR="false"
                fi
                #echo isup
        else
                if [ "$VAR" = "false" ]
                then
                        echo Response failure, failing over...
                        /etc/init.d/firewall restart
                        VAR="true"
                fi
        fi
        sleep 3
done;
