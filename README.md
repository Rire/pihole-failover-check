# pihole-failover-check

A script that forces traffic from your pihole DNS to your router's DNS if the pihole server is down.

This is intended for OpenWRT devices, but feel free to tweak this for other router firmware.

# Usage

The webui is required to be enabled in order for this to be able to check if your pihole is online remotely.

Install the coreutils-nohup and curl package:

`# opkg update && opkg install coreutils-nohup curl`

place init-checkfw in /etc/init.d

place checkfw.sh in /root

exec `# /etc/init.d/init-checkfw start`
